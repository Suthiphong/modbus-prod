use amr_modbus_prod::modbus::modbus::modbus;
use amr_modbus_prod::modbus::modbus::EvcType;
use serde::Deserialize;
use std::{error::Error, io, process};

#[derive(Deserialize, Debug)]
struct IPAddress {
    ip_with_port: String,
    device_type: String,
}

fn main() {
    let mut rdr = csv::Reader::from_path("iptable.csv").unwrap();
    for result in rdr.deserialize() {
        let record: IPAddress = result.unwrap();
        let device_type = record.device_type.as_str();
        let device_type: Option<EvcType> = match device_type {
            "EK" => Some(EvcType::EK),
            "AT" => Some(EvcType::AT),
            _ => {
                println!("Address:{:?}", record);
                println!("found type miss match");
                None
            }
        };

        if device_type.is_some() {
            let mut m = modbus::new(&record.ip_with_port, device_type.unwrap());
            m.hallelujah();
        }

        println!("====================================")
    }
}
