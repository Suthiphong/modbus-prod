use std::io::{Read, Write};
use std::net::TcpStream;
use std::time::SystemTimeError;
use std::{thread, time};

pub struct modbus {
    ipaddress_with_port: String,
    device_type: EvcType,
}

#[derive(Debug, Clone)]
pub enum EvcType {
    EK,
    AT,
}

impl modbus {
    pub fn new(ipaddress_with_port: &str, device_type: EvcType) -> Self {
        Self {
            ipaddress_with_port: ipaddress_with_port.to_string(),
            device_type,
        }
    }

    pub fn hallelujah(&mut self) {
        let mut stream = TcpStream::connect(self.ipaddress_with_port.as_str());
        match stream {
            Ok(mut s) => {
                s.set_read_timeout(Some(time::Duration::from_secs(5)))
                    .expect("Error setting timeout.");
                println!("Address:{} is connected", self.ipaddress_with_port);
                match self.device_type {
                    EvcType::EK => {
                        println!("polling from EK type");
                        /*
                            Example data output:
                            [00, 01, 00, 00, 00, 0F, 01, 03, 0C, 3F, 19, 09, 6C, 3F, CE, 14, 7B, 3F, BE, 56, 04, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00]
                        */
                        let modbus_command = [0x01, 0x03, 0x03, 0xEA, 0x00, 0x06, 0xE4, 0x78];
                        let header: [u8; 8] = [0xFF, 0xFD, 0x30, 0xFF, 0xFB, 0x30, 0x0D, 0x0A];

                        //thread::sleep(time::Duration::from_millis(500));
                        //s.write(&header).expect("Header write error");
                        thread::sleep(time::Duration::from_millis(500));
                        s.write(&modbus_command).expect("Write data error");
                        thread::sleep(time::Duration::from_millis(500));

                        let mut response: [u8; 100] = [0; 100];
                        match s.read(&mut response) {
                            Ok(v) => {
                                let bytes1: Result<[u8; 4], _> = response[3..=6].try_into();
                                let bytes2: Result<[u8; 4], _> = response[7..=10].try_into();
                                let bytes3: Result<[u8; 4], _> = response[11..=14].try_into();
                                println!("{:2X?}", response);

                                match bytes1 {
                                    Ok(v) => {
                                        println!(
                                            "40003: {}",
                                            f32::from_bits(u32::from_be_bytes(v))
                                        );
                                    }
                                    Err(e) => {
                                        println!("{:?}", e)
                                    }
                                }

                                match bytes2 {
                                    Ok(v) => {
                                        println!(
                                            "40005: {}",
                                            f32::from_bits(u32::from_be_bytes(v))
                                        );
                                    }
                                    Err(e) => {
                                        println!("{:?}", e)
                                    }
                                }

                                match bytes3 {
                                    Ok(v) => {
                                        println!(
                                            "40007: {}",
                                            f32::from_bits(u32::from_be_bytes(v))
                                        );
                                    }
                                    Err(e) => {
                                        println!("{:?}", e)
                                    }
                                }
                            }
                            Err(err) => {
                                println!("Timeout: 5s");
                            }
                        }
                    }
                    EvcType::AT => {
                        /*
                            Example data output:
                        */
                        println!("polling from AT type");
                        let header: [u8; 8] = [0xFF, 0xFD, 0x30, 0xFF, 0xFB, 0x30, 0x0D, 0x0A];
                        let modbus_command: [u8; 8] =
                            [0x01, 0x03, 0x00, 0x20, 0x00, 0x04, 0x45, 0xC3];

                        thread::sleep(time::Duration::from_millis(500));
                        s.write(&header).expect("Header write error");
                        thread::sleep(time::Duration::from_millis(500));
                        s.write(&modbus_command).expect("Write data error");
                        thread::sleep(time::Duration::from_millis(500));

                        let mut response = [0; 30];
                        match s.read(&mut response) {
                            Ok(v) => {
                                println!("{:2X?}", response);

                                let bytes1: Result<[u8; 4], _> = response[3..7].try_into();
                                let bytes2: Result<[u8; 4], _> = response[7..11].try_into();
                                match bytes1 {
                                    Ok(v) => {
                                        println!(
                                            "40033: {}",
                                            f32::from_bits(u32::from_be_bytes(v))
                                        );
                                    }
                                    Err(e) => {
                                        println!("{:?}", e)
                                    }
                                }

                                match bytes2 {
                                    Ok(v) => {
                                        println!(
                                            "40035: {}",
                                            f32::from_bits(u32::from_be_bytes(v))
                                        );
                                    }
                                    Err(e) => {
                                        println!("{:?}", e)
                                    }
                                }
                            }
                            Err(v) => {
                                println!("timeout: 5sec.");
                            }
                        }
                    }
                }
            }
            Err(error) => {
                println!("Something error");
                println!("{:?}", error);
            }
        }
    }
}
